import { Text } from '@chakra-ui/react';
import shallow from 'zustand/shallow';
import { useTodos } from '../store/todoStore';

export const TotalTodos = () => {
  const count = useTodos((state) => state.todos.length, shallow);

  return <Text fontWeight="bold">Total: {count}</Text>;
};
