import { Button } from '@chakra-ui/react';
import { useTodos } from '../store/todoStore';
import shallow from 'zustand/shallow';

export const FetchTodo = () => {
  const { isLoading, error, fetchTodos } = useTodos(
    (state) => ({
      isLoading: state.isLoading,
      error: state.error,
      fetchTodos: state.fetchTodos,
    }),
    shallow
  );

  return (
    <Button isLoading={isLoading} onClick={() => fetchTodos()}>
      {!error ? 'Get todos' : error}
    </Button>
  );
};
