import { Checkbox, HStack, Stack, Text } from '@chakra-ui/react';
import { FC, useEffect } from 'react';
import { useFilter } from '../store/filterStore';
import { useTodos } from '../store/todoStore';
import { ITodo } from '../types/ITodo';
import shallow from 'zustand/shallow';

const Todo: FC<ITodo> = ({ id, title, completed }) => {
  const toggleCompleted = useTodos((state) => state.toggleTodo, shallow);

  return (
    <HStack spacing="4">
      <Checkbox isChecked={completed} onChange={() => toggleCompleted(id)} />
      <Text>{title}</Text>
    </HStack>
  );
};

export const TodoList = () => {
  const filter = useFilter((state) => state.filter, shallow);
  const fetchTodos = useTodos((state) => state.fetchTodos, shallow);
  const todos: ITodo[] = useTodos((state) => {
    switch (filter) {
      case 'completed':
        return state.todos.filter((todo) => todo.completed);

      case 'uncompleted':
        return state.todos.filter((todo) => !todo.completed);

      default:
        return state.todos;
    }
  }, shallow);

  return (
    <Stack minH="300px">
      {!todos.length && <Text fontWeight="bold">Not found todo</Text>}
      {todos.map((todo) => (
        <Todo key={todo.id} {...todo} />
      ))}
    </Stack>
  );
};
