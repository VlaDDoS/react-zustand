import {
  Button,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  Input,
  useDisclosure,
} from '@chakra-ui/react';
import React, { useState } from 'react';
import shallow from 'zustand/shallow';
import { useTodos } from '../store/todoStore';

export const NewTodo = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [value, setValue] = useState<string>('');
  const addTodo = useTodos((state) => state.addTodo, shallow);

  const handleAddTodo = () => {
    addTodo(value);
    setValue('');
    onClose();
  };

  return (
    <>
      <Button colorScheme="teal" onClick={onOpen}>
        Add new Todo
      </Button>

      <Drawer isOpen={isOpen} placement="right" onClose={onClose}>
        <DrawerOverlay />

        <DrawerContent>
          <DrawerCloseButton />
          <DrawerHeader>Create new todo</DrawerHeader>

          <DrawerBody>
            <Input
              placeholder="Type here ..."
              value={value}
              onChange={(e) => setValue(e.target.value)}
              onKeyDown={(e) => e.key === 'Enter' && handleAddTodo()}
              autoFocus
            />
          </DrawerBody>

          <DrawerFooter>
            <Button variant="outline" mr="3" onClick={onClose}>
              Cancel
            </Button>
            <Button colorScheme="blue" onClick={handleAddTodo}>
              Save
            </Button>
          </DrawerFooter>
        </DrawerContent>
      </Drawer>
    </>
  );
};
