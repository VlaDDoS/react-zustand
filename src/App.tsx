import { Divider, VStack } from '@chakra-ui/react';
import { FetchTodo } from './components/FetchTodo';
import { Filter } from './components/Filter';
import { NewTodo } from './components/NewTodo';
import { TodoList } from './components/TodoList';
import { TotalTodos } from './components/TotalTodos';

export const App = () => {
  return (
    <VStack spacing={4}>
      <Filter />
      <TodoList />
      <Divider />
      <TotalTodos />
      <NewTodo />
      <FetchTodo />
    </VStack>
  );
};
