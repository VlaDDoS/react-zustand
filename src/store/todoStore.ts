import create from 'zustand';
import { devtools, persist } from 'zustand/middleware';
import { ITodo } from '../types/ITodo';

interface TodoState {
  todos: ITodo[];
  isLoading: boolean;
  error: string;

  addTodo: (title: string) => void;
  toggleTodo: (todoId: number) => void;
  fetchTodos: () => void;
}

export const useTodos = create<TodoState>()(
  devtools((set, get) => ({
    todos: [],

    isLoading: false,

    error: '',

    addTodo: (title) => {
      const newTodo: ITodo = {
        id: Date.now(),
        title,
        completed: false,
      };

      set({ todos: [newTodo, ...get().todos] });
    },

    toggleTodo: (todoId) => {
      set({
        todos: get().todos.map((todo) =>
          todo.id === todoId ? { ...todo, completed: !todo.completed } : todo
        ),
      });
    },

    fetchTodos: async () => {
      set({ isLoading: true });

      try {
        const response = await fetch(
          'https://jsonplaceholder.typicode.com/todos?_limit=20'
        );

        const data = await response.json();

        set({ todos: [...get().todos, ...data], error: '' });
      } catch (e) {
        if (e instanceof Error) {
          set({ error: e.message, todos: [] });
        }
      } finally {
        set({ isLoading: false });
      }
    },
  }))
);
