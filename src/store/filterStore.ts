import create from 'zustand';

export type FilterType = 'all' | 'uncompleted' | 'completed';

interface FilterStore {
  filter: FilterType;
  setFilter: (value: FilterType) => void;
}

export const useFilter = create<FilterStore>()((set) => ({
  filter: 'all',
  setFilter: (value) => set({ filter: value }),
}));
